# Simple Table and Form Generic Template

This project contains generic components of a form and table, and a generic crud service.

# Usage

## Table and form

A full runnable example is in `examples/example`.

To use these templates you need to create an `interface` of the type you wish to display, a `service` which implements `ObjectArrayCrudService<InterFace>`, and finally the `component` which displays `<app-generic-form>` and `<app-generic-table>`.

There is documentation inside of `examples/example/example.component.html` on how to use both selectors.

## Dropdown selector

A dropdown selector allows you to link the values of one table with a `<select><option>` tag of another.

Firstly you define an object with a desired type inside of your interface which will hold the dropdown.

```Typescript
secondInterface.ts

export interface SecondInterface {
    nestedValue:string
}
```

```Typescript
firstInterface.ts

export interface FirstInterface {
    some_value:string,
    other_value:SecondInterface
}
```

Then you define a service and a tagType value of `type:"dropdown"`.

```Typescript
first.component.ts

tagTypes:ValueTagTypePair[] = [
    {tag:"value", type:"dropdown", nest:"nestedValue"},
]
```

Then create a `Dropdown` array with one or more `Dropdown` objects.

```Typescript
first.component.ts

constructor(public firstService:FirstService, public secondService:SecondService)

firstObject = this.firstService.getDefault();

titles = ['Some Value', 'Other Value']

firstDropdown:Dropdown = {
    labelText: "Your dropdown selector",
    name: "other_value",
    valueToDisplay: "some_value",
    linkedObject: this.firstObject,
    linkedArray: this.secondService.getAll()
}

dropdowns:Dropdown[] = [firstDropdown]
```
You can leave out the `valueToDisplay` option if you are showing a simple variable. It is only useful if you have an interface which has multiple values it can display.

Finally link everything up in HTML.

```HTML
first.component.html

<app-generic-form [serviceObject]="firstService" [titles]="titles" [valueTagsTypes]="tagTypes" [default_object]="firstObject" [dropdowns]="dropdowns"></app-generic-form>

<app-generic-table [serviceObject]="firstService" [columnNames]="titles" [columnValueTagTypes]="tagTypes" [actions]="[]"></app-generic-table>
```

## Sizes too small

If the table is too small, you can modify it's width in `app/components/generic-table/generic-table.component.css` in the `#tableContainer` id.

The same goes for the form, in `app/components/generic-form/generic-table.component.css` inside the `form` category.