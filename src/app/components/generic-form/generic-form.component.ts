import { Component, Input, OnInit } from '@angular/core';
import { Dropdown } from 'src/app/model/dropdown';
import { ValueTagTypePair } from 'src/app/model/value-tag-type-pair';

@Component({
  selector: 'app-generic-form',
  templateUrl: './generic-form.component.html',
  styleUrls: ['./generic-form.component.css']
})
export class GenericFormComponent implements OnInit {
  @Input()
  serviceObject:any = [];
  @Input()
  titles:string[] = [];
  @Input()
  valueTagsTypes:ValueTagTypePair[] = [];
  @Input()
  default_object:any = {}
  @Input()
  dropdowns:Dropdown[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  create() {
    console.log(this.default_object);
    this.serviceObject.insert({...this.default_object});
  }

}
