import { Component, Input, OnInit } from '@angular/core';
import { ActionPair } from 'src/app/model/action-pair';
import { ValueTagTypePair } from 'src/app/model/value-tag-type-pair';

@Component({
  selector: 'app-generic-table',
  templateUrl: './generic-table.component.html',
  styleUrls: ['./generic-table.component.css']
})
export class GenericTableComponent implements OnInit {
  @Input()
  serviceObject:any = [];
  @Input()
  columnNames:string[] = [];
  @Input()
  columnValueTagTypes:ValueTagTypePair[] = [];
  @Input()
  actions:ActionPair[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
