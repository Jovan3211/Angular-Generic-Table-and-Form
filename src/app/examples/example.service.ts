import { Injectable } from '@angular/core';
import { ObjectArrayCrudService } from '../services/object-array-crud.service';
import { ExampleInterface } from './example-interface';

@Injectable({
  providedIn: 'root'
})
export class ExampleService extends ObjectArrayCrudService<ExampleInterface> {

  getDefault(): ExampleInterface {
    return {id:0, name:"", address:"", email:"", valued:""}
  }

  exampleObjects: ExampleInterface[] = [
    {id:1, name:"Michael", address:"505 New York", email:"michael@gmail.com", valued:"Yes"},
    {id:2, name:"Maurice", address:"32 Tokyo", email:"maurice@gmail.com", valued:"Yes"},
    {id:3, name:"Shawty", address:"26 Nis", email:"shawty@of.com", valued:"No"},
    {id:4, name:"Candice", address:"Deez Nutts", email:"candice@yahoo.com", valued:"No"},
    {id:5, name:"Mom", address:"666 Hell", email:"mom@aol.com", valued:"Yes"}
  ]

  constructor() {
    super();
    super.objectArray = this.exampleObjects // Premade objects are set into the object array on construction
  }
}
