import { Component, OnInit } from '@angular/core';
import { ActionPair } from 'src/app/model/action-pair';
import { Dropdown } from 'src/app/model/dropdown';
import { ValueTagTypePair } from 'src/app/model/value-tag-type-pair';
import { ExampleService } from '../example.service';

@Component({
  selector: 'app-example',
  templateUrl: './example.component.html',
  styleUrls: ['./example.component.css']
})
export class ExampleComponent implements OnInit {

  test() {
    console.log("A button has been pressed.")
  }

  constructor(public exampleService:ExampleService) { }

  prettyTitles = ['Id', 'Name', 'Home Address', 'E-Mail', "Valued Employee"]   // The names that are shown for column headers and form prompts.
  default_example = this.exampleService.getDefault();       // Default dictionary which is used in the creation form.

  example_tagTypes:ValueTagTypePair[] = [                   // A TagTypePair is a dictionary which needs to correspond
    {tag: "id", type: "number"},                            // to the interface definition of the object which is
    {tag: "name", type: "string"},                          // represented in the table.
    {tag: "address", type: "string"},
    {tag: "email", type: "string"},
    {tag: "valued", type: "dropdown"}
  ]

  valuedArray = ["Yes", "No"]

  example_dropDown:Dropdown = {
    labelText:"Select an option",
    name:"valued",
    linkedObject:this.default_example,
    linkedArray:this.valuedArray
  }

  dropdowns:Dropdown[] = [this.example_dropDown]

  actions:ActionPair[] = [                                  // Custom button actions are defined as ActionPairs.
    {actionFunction: this.test, buttonTitle: "A Button"}    // They have the function pointer, and a title to be displayed
  ]                                                         // inside the button.

  ngOnInit(): void {
  }

}
