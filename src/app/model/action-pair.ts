export interface ActionPair {
    buttonTitle:string,
    actionFunction: () => any;
}
