export interface Dropdown {
    labelText:string,               //Text displayed above dropdown menu
    name:string,                    //Name of the tag which to modify inside of linkedObject (linkedObject.name)
    valueToDisplay?:string,         //Which variable to display of linked service result (linkedService.getAll() -> object[valueToDisplay]); If null display object directly
    linkedObject:any,               //Object which is to be modified upon selection
    linkedArray:any[],              //Linked array from which objects are selected
}