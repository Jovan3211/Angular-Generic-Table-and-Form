export interface ValueTagTypePair {
    tag:string,
    type:string,
    nest?:string    // A very naive quick fix to a problem of nested id's: example["person"]["name"]
}
