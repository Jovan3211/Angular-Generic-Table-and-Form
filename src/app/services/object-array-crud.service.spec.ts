import { TestBed } from '@angular/core/testing';

import { ObjectArrayCrudService } from './object-array-crud.service';

describe('ObjectArrayCrudService', () => {
  let service: ObjectArrayCrudService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ObjectArrayCrudService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
