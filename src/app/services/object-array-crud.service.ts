import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export abstract class ObjectArrayCrudService<Type> {

  objectArray:Type[] = []

  constructor() { }

  abstract getDefault():Type;

  getAll() {
    return this.objectArray;
  }

  get(index:number) {
    return this.objectArray[index];
  }

  /* Try to figure out how to pass obj as a dictionary to use key in
  getByKey(key:string, value:any) {
    for(let obj of this.objectArray) {
      if (obj[key] == value) {
        return obj;
      }
    }

    return null;
  }
  */

  insert(obj:Type) {
    this.objectArray.push(obj);
  }

  update(index:number, new_object:Type) {
    if (this.objectArray.length-1 < index) {
      return false;
    }

    this.objectArray[index] = new_object;
    return true;
  }

  delete(index:number) {
    if (this.objectArray.length-1 < index) {
      return false;
    }

    this.objectArray.splice(index, 1);
    return true;
  }
}
